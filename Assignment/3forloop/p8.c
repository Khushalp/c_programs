
//WAP take two characters if these characters are equal then print the same but if
//they are unequal then print their difference....

#include<stdio.h>

void main() {

	char ch1, ch2;

	printf("Enter two character to find the difference : \n");
	scanf(" %c %c",&ch1,&ch2);

	if( ch2 > ch1 )
		printf("%d is the difference between %c and %c \n",ch2-ch1,ch2,ch1);
	
	else if( ch1 > ch2 )
		printf("%d is the difference between %c and %c \n",ch1-ch2,ch1,ch2);

	else
		printf("Both character are equal\n");

}
