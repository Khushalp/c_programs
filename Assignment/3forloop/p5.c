
//WAP to take the Number input and print all the factors of that number

#include<stdio.h>

void main() {

	int num = 0;

	printf("Enter a number  : ");
	scanf("%d",&num);

	printf("Factor of number are : ");

	int div = 0;

	for(int i = 1; i <= num; i++ ) {
	
		if( num % i == 0 ) {

			printf("%d ",i);
		}
	}

	printf("\n");
}
