
//WAP to calculate the LCM of given two numbers

#include<stdio.h>

void main() {

	int num1 = 0, num2 = 0;

	printf("Enter two number to find LCM : \n");
	scanf("%d%d",&num1,&num2);

	for(int i = 1; i <= (num1*num2); i++ ) {
	
		if((i % num1 == 0) && (i % num2 == 0)) {

			printf("LCM of %d and %d is %d\n",num1,num2,i);
			break;
		}
	}

}
