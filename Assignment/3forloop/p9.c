
//WAP to calculate the square root of a number ranging from 100 to 300

#include<stdio.h>

void main() {

	for(int i = 100; i <= 300; i++ ) {
	
		int j = 1;

		while( j <= 100 ) {

			if((i/j == j) && (i%j == 0))
				printf("Square root of %d is %d\n",i,j);

			j++;
		}
	}
}

