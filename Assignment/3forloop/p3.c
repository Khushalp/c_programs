
//WAP to print all even numbers in reverse order and odd numbers in the standard
//way. Both separately. Within a range.

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	printf("Even number in reverse order : \n");

	for(int i = range; i >= 1; i-- ) {
	
		if(i % 2 == 0) 
			printf("%d ",i);
	}

	printf("\n");

	printf("Odd number in standard order : \n");

	for(int i = 1; i <= range; i++ ) {
	
		if( i % 2 == 1)
			printf("%d ",i);
	}

	printf("\n");
}
