
//WAP to Find the sum of numbers that are divisible by 5 in the given range

#include<stdio.h>

void main() {

	int sum = 0,range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	printf("Numbers divisible by 5 are : ");

	for(int i = 1; i <= range; i++ ) {
	
		if( i % 5 == 0 ) {
		
			printf("%d ",i);
			sum += i;
		}
	}

	printf("\n");
	printf("%d is sum of numbers that are divisible by 5\n",sum);
}
