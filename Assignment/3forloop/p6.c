
//WAP to calculate the factorial of a given number.

#include<stdio.h>

void main() {

	int num = 0;

	printf("Enter a number : ");
	scanf("%d",&num);

	int fact = 1;

	for(int i = num; i >= 1; i-- )
		fact *= i;

	printf("%d is factorial of %d\n",fact,num);

}
