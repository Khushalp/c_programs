
/*
 If possible take no of rows from the user

	A B C D
	B C D E 
	C D E F
	D E F G
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	for(char i = 'A'; i < 'A'+range; i++ ) {
	
		char ch = i;

		for(int j = 0; j < 4; j++ ) {
		
			printf("%c ",ch++);
		}

		printf("\n");
	}
}
