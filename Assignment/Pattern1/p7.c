
/*
If possible take no of rows from the user

	1	2	9	4
	25	6	49	8
	81	10	121	12
	169	14	225	16
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter number of rows : ");
	scanf("%d",&range);

	int num = 1;

	for(int i = 1; i <= range; i++ ) {
	
		for(int j = 1; j <= 4; j++ ) {
		
			if(j % 2 != 0) 
				printf("%d ",num*num);
			else
				printf("%d ",num);

			num++;
		}

		printf("\n");
	}
}
