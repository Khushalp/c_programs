
/*
 * If possible take no of rows from the user
	
 	1 1 1 1
	2 2 2 2
	3 3 3 3
	4 4 4 4
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	for(int i = 1; i <= range; i++ ) {
	
		for(int j = 0; j < 4; j++ ) {
		
			printf("%d ",i);
		}

		printf("\n");
	}
}
