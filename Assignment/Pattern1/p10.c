
/*
If possible take no of rows from the user

	D4  C3  B2  A1
	D4  C3  B2  A1
	D4  C3  B2  A1
	D4  C3  B2  A1
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter no. of rows : ");
	scanf("%d",&range);

	for(int row = range; row > 0; row-- ) {
	
		int x = range;

		for(char col = range+64; col > 64; col--) {
		
			printf("%c%d ",col,x--);
		}

		printf("\n");
	}
}
