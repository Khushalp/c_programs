
/*
 * If possible take no of rows from the user
	1 2 3 4
	2 3 4 5
	3 4 5 6
	4 5 6 7
 */

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter no. of rows : ");
	scanf("%d",&range);

	for( int row = 1; row <= range; row++ ) {
	
		int x = row;

		for(int col = 0; col < range; col++ ) {
		
			printf("%d ",x);
			x++;
		}

		printf("\n");
	}

	printf("\n");
}
