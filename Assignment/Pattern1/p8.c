
/*
If possible take no of rows from the user

	1  3  5
	7  9  11
	13 15 17
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter number of rows : ");
	scanf("%d",&range);

	int num = 1;

	for(int i = 0; i < range; i++ ) {
	
		for(int j = 0; j < 3; j++ ) {
		
			printf("%d ",num);
			num += 2;
		}

		printf("\n");
	}
}
