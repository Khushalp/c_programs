
/*
 If possible take no of rows from the user
 	1 2 3 
	a b c
	1 2 3
	a b c
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	for( int row = 1; row <= range; row++ ) {
	
		int x = 1;

		for(char col = 'a'; col <= 'c'; col++ ) {

			if( row % 2 != 0 ) {
			
				printf("%d ",x);
				x++;
			
			} else {
			
				printf("%c ",col);
			}
		}

		printf("\n");
	} 
}
