
/**
 *If possible take no of rows from the user

 	A B C 
	D E F
	G H I 
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	char ch = 'A';

	for(int i = 0; i < range; i++) {
	
		for(int j = 0; j < 3; j++ ) {
		
			printf("%c ",ch++);
		}

		printf("\n");
	}
}
