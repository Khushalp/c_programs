
/*
If possible take no of rows from the user

	2  5  10
	17 26 37
	50 65 82
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	int num = 1;

	for(int i = 0; i < range; i++ ) {
	
		for(int j = 0; j < range; j++ ) {
		
			printf("%d ",num*num+1);
			num++;
		}

		printf("\n");
	}
}
