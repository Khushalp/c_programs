
/*
 *take no of rows from the user

	=	=	=	=	=	=
	$	$	$	$	$	$
	@	@	@	@	@	@
	=	=	=	=	=	=
	$	$	$	$	$	$
	@	@	@	@	@	@
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	int x = 0;

	for(int i = 0; i < range; i++ ) {
	
		for(int j = 0; j < range; j++ ) {
		
			if(i == 0 || x%3 == 0) 
				printf("=\t");
			else if(i == 1 || x%4 == 0)
				printf("$\t");
			else
				printf("@\t");
		}

		printf("\n");
		x++;
	}
}
