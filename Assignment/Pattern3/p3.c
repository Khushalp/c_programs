
/*
 * take no of rows from the user

	4	a	3	b
	4	a	3	b
	4	a	3	b
	4	a	3	b
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	for(int i = 0; i < range; i++ ) {
	
		char ch = 'a';
		int x = range;

		for(int j = 0; j < range; j++) {
		
			if(j % 2 == 0)
				printf("%d\t",x--);
			else
				printf("%c\t",ch++);
		}

		printf("\n");
	}
}
