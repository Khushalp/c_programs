
/*
 * take no of rows from the user

	1	4	3
	16	5	36
	7	64	9
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	int x= 1;

	for(int i = 0; i < range; i++ ) {
	
		for(int j = 0; j < range; j++ ) {
		
			if((i+j)%2 != 0)
				printf("%d\t",x*x);
			else
				printf("%d\t",x);

			x++;
		}

		printf("\n");
	}
}
