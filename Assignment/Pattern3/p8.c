
/*
 *take no of rows from the user

	16	15	14	13
	L	K	J	I
	8	7	6	5
	D	C	B	A
*/

#include<stdio.h>

void main() {

	int range = 0;
	
	printf("Enter range below 5 or 5 : ");
	scanf("%d",&range);

	int x = range*range;
	char ch = 64+x;

	for(int i = 0; i < range; i++ ) {
	
		for(int j= 0; j < range; j++ ) {
		
			if( i%2 != 0 )
				printf("%d\t",x);
			else
				printf("%c\t",ch);

			x--;
			ch--;
		}

		printf("\n");
	}
}
