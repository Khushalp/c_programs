
/*
 *take no of rows from the user

	D1	C2	B3	A4
	e5	f4	g3	h2	
	F3	E4	D5	C6
	g7	h6	i5	j4
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	char ch1 = 64+range;

	for(int i = 0; i < range; i++ ) {
	
		int x = i+1;
		int y = range+i;

		for(int j = range; j >= 1; j-- ) {

			char ch2 = ch1;
		
			if( i % 2 == 0 ) {
			
				printf("%c%d\t",ch1--,x++);
			
			} else {

				ch2 = ch1 + 33; 
				printf("%c%d\t",ch2++,y--);
			}
		}

		printf("\n");
	}
}

