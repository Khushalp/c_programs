
/*
take no of rows from the user
	
	1 2 3 4
	1 3 5 7
	1 4 7 10
	1 5 9 13
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("ENter range : ");
	scanf("%d",&range);

	for(int i = 1; i <= range; i++ ) {
	
		int x = 1;

		for(int j = 0; j < range; j++ ) {

			printf("%d ",x);
			x += i;
		}

		printf("\n");
	}
}

