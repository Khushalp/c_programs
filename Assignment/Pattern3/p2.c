
/*
 * take no of rows from the user

	3	b	1	d
	a	2	c	0
	3	b	1	d
	a	2	c	0
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	for(int i = 0; i < range; i++ ) {

		char ch = 'a';
		int x = range-1;
	
		for(int j = 0; j < range; j++ ) {
		
			if((j+i)%2 == 0)
				printf("%d\t",x);
			else
				printf("%c\t",ch);

			ch++;
			x--;
		}

		printf("\n");
	}
}
