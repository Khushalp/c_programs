
/*
 * take no of rows from the user

	1	4	27
	4	27	16
	27	16	125
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	for(int i = 1; i <= range; i++ ) {
	
		int x = i;

		for(int j = 0; j < range; j++ ) {
		
			if(x%2 == 0)
				printf("%d\t",x*x);
			else
				printf("%d\t",x*x*x);

			x++;
		}

		printf("\n");
	}
}
