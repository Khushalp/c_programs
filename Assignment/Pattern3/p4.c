
/*
 * take no of rows from the user

	a	B	c	D
	b	C	d	E
	c	D	e	F
	d	E	f	G
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	for(char row = 97; row <= range+96; row++ ) {
	
		char ch = row;
		char ch1 = row-32;

		for(int col = 0; col < range; col++ ) {
		
			if( col%2 == 0) {

				printf("%c\t",ch);
			
			} else {
			
				printf("%c\t",ch1);
			}

			ch++;
			ch1++;
		}

		printf("\n");
	}
}
