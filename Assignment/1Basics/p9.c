
//Write a program to print the sum of the first 10 odd numbers

#include<stdio.h>

void main() {

	int oddnum = 0;

	for(int i = 1; i <= 10; i++ ) {
	
		if(oddnum % 2 == 1) {
			
			printf("%d ",oddnum);
			oddnum++;

		} else {
		
			oddnum++;
			i--;
		}
	}

	printf("\n");
}
