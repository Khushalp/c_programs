
//Write a program to print the first ten, 3 digit number

#include<stdio.h>

void main() {

	int x = 0;

	for(int i = 0; i < 10; i++ ) {

		if( x >= 100 && x <= 999 ) {
		
			printf("%d ",x);
			x++;
		
		} else {
		
			i--;
			x++;
		}
	}

	printf("\n");
}

