
//Write a program to print the first 100 numbers

#include<stdio.h>

void main() {

	int start = 0;

	printf("Enter number to start : ");
	scanf("%d",&start);

	for(int i = start; i < (100+start); i++ ) {
	
		printf("%d ",i);
	}

	printf("\n");
}
