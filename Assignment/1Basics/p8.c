
//Write a program to print a table of 11 in reverse order

#include<stdio.h>

void main() {

	int num = 0;

	printf("Enter number to prints reverse table : ");
	scanf("%d",&num);

	for(int i = 10; i >= 1; i-- )
		printf("%d * %d = %d\n",num,i,num*i);
}
