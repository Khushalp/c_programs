
/*take numerical input from the user and find whether the number is
divisible by 5 and 11*/

#include<stdio.h>

void main() {

	int num;

	printf("Enter a number : \n");
	scanf("%d",&num);

	if((num % 11 == 0) && (num % 5 == 0))
		printf("%d is divisible by 5 and 11\n",num);

	else
		printf("%d is not divisible by 5 and 11\n",num);
}
