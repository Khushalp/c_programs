
/*take a number and print whether it is less than 10 or greaterthan 10*/

#include<stdio.h>

void main() {

	int val;

	printf("Enter a value : ");
	scanf("%d",&val);

	if( val > 10 ) 
		printf("%d is greater than 10\n",val);

	else if( val < 10 )
		printf("%d is smaller than 10\n",val);
	
	else
		printf("%d is equal to 10\n",val);
}
