
//program to check if a number is even or odd

#include<stdio.h>

void main() {

	int num;

	printf("Enter number to check it is even or odd : \n");
	scanf("%d",&num);

	if( num % 2 == 0 )
		printf("%d is even number\n",num);

	else
		printf("%d is odd number\n",num);
}
