
#include<stdio.h>

void main() {

	int x = 9;

	int ans = ++x + x++ + ++x;

	printf("x = %d\n",x);		//12
	printf("ans = %d\n",ans);	//33

	int ans1 = ++x + ++x + ++x + ++x;

	printf("x = %d\n",x);		//16
        printf("ans1 = %d\n",ans1);	//59

	int ans2 = x++ + x++ + ++x + x++ + ++x;

	printf("x = %d\n",x);		//21
        printf("ans2 = %d\n",ans2);	//92

	int ans3 = x++ + x++ + x++ + x++;

	printf("x = %d\n",x);		//25
        printf("ans3 = %d\n",ans3);	//90

}
