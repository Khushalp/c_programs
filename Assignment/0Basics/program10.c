
/*
program to check if a character is a vowel or
consonant
*/

#include<stdio.h>

void main() {

	char ch;

	printf("ENter a character : ");
	scanf("%c",&ch);

	if( ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' ) {
	
		printf("%c is a vowel character\n",ch);
	
	} else if( ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U' ) {

                printf("%c is a vowel character\n",ch);

        } else {
	
		printf("%c is a consonant character\n",ch);
	}
}
