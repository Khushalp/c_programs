
/*
take no of rows from the user

	D4  C3  B2  A1
	A1  B2  C3  D4
	D4  C3  B2  A1
	A1  B2  C3  D4
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter number of rows : ");
	scanf("%d",&range);

	for(int i = 0; i < range; i++ ) {
	
		int x = 4;
		char ch = 65;

		for(int j = 1; j <= 4; j++ ) {
		
			if(i%2==0){
				
				ch = 69 - j;
				printf(" %c%d ",ch,x--);
			
			} else {
			
				printf(" %c%d ",ch++,j);
			}
		}

		printf("\n");
	}
}

