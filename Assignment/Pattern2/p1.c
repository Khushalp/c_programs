
/*
take no of rows from the user
	
	4 3 2 1
	5 4 3 2
	6 5 4 3
	7 6 5 4 
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter number of rows : ");
	scanf("%d",&range);

	int x = 4;

	for(int i = 0; i < range; i++ ) {

		int y = x;
		
		for(int j = 1; j <= 4; j++ ) {
		
			printf("%d ",y--);
		}

		x++;
		printf("\n");
	}
}
