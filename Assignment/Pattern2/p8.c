
/*
take no of rows from the user

	18  16  14
	12  10  8
	6   4   2
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter row number : ");
	scanf("%d",&range);

	int x = range*6;

	for(int i = 0; i < range; i++ ) {
	
		for(int j = 0; j < 3; j++ ) {
		
			printf("%d ",x);
			x -= 2;
		}

		printf("\n");
	}
}
