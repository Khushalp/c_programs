
/*
take no of rows from the user

	3 2 1
	c b a
	3 2 1
	c b a
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter no. of rows : ");
	scanf("%d",&range);

	for(int i = range; i > 0; i-- ) {
	
		int x = 3;

		for(char j = 'c'; j >= 'a'; j-- ) {
		
			if( i % 2 == 0 )
				printf("%d ",x--);
			else
				printf("%c ",j);
		}

		printf("\n");
	}
}
