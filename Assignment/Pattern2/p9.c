
/*
take no of rows from the user

	0   3   8
	15  24  35
	48  63  80
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter row no : ");
	scanf("%d",&range);

	int x = 0;

	for(int i = 0; i < range; i++ ) {
	
		for(int j = 0; j < range; j++ ) {
		
			++x;
			printf("%d ",(x*x)-1);
		}

		printf("\n");
	}
}
