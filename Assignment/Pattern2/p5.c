
/*
take no of rows from the user

	D C B A
	e d c b
	F E D C
	g f e d
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter no of row : ");
	scanf("%d",&range);

	int temp = range;

	for(int i = 0; i < range; i++ ) {
	
		char ch = temp + 64;
		char ch2 = ch + 32;

		for(int j = 0; j < range; j++ ) {
		
			if( i % 2 == 0 )
				printf("%c ",ch--);
			else
				printf("%c ",ch2--);
		}

		temp++;
		printf("\n");
	}
}
