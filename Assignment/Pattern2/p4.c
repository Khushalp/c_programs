
/*
take no of rows from the user

	I H G
	F E D
	C B A
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter number of rows : ");
	scanf("%d",&range);

	char ch = 64+(range*range);

	for(int row = 0; row < range; row++ ) {
	
		for(int col = 0; col < range; col++ ) {
		
			printf("%c ",ch--);
		}

		printf("\n");
	}
}
