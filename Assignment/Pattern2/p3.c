
/*
take no of rows from the user

	4 4 4 4
	3 3 3 3
	2 2 2 2
	1 1 1 1
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	for(int row = range; row > 0; row-- ) {
	
		for(int col = 1; col <= 4; col++ ) {
		
			printf("%d ",row);
		}

		printf("\n");
	}
}
