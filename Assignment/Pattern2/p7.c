
/*
take no of rows from the user
	
	1	2	3	4   
	25	36	49	64
	9	10	11	12
	169	196	225	256
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	int x = 1;

	for(int i = 0; i < range; i++ ) {
	
		for(int j = 0; j < 4; j++ ) {
		
			if(i%2==0)
				printf("%d ",x);
			else
				printf("%d ",x*x);

			x++;
		}

		printf("\n");
	}
}
