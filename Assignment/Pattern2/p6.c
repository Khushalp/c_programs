
/*
take no of rows from the user

	= = = =
	$ $ $ $
	= = = =
	$ $ $ $
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter row no. : ");
	scanf("%d",&range);

	for(int i = 0; i < range; i++ ) {
	
		for(int j = 0; j < 4; j++ ) {
		
			if(i%2==0)
				printf("= ");
			else
				printf("$ ");
		}

		printf("\n");
	}
}
