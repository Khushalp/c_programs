//WAP to allocate memeory of integer type and then free it without using free() function

#include<stdio.h>
#include<stdlib.h>

void main() {

	int *ptr1 = (int*)malloc(sizeof(int));
	*ptr1 = 10;

	printf("ptr1 = %d\n",*ptr1);

	int *ptr2 = (int*)realloc(ptr1,0);

	printf("ptr1 = %d\n",*ptr1);

	*ptr2 = *ptr1;

	printf("ptr2 = %d\n",*ptr2);

}
