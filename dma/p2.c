
//WAP to show the relation of dangling pointer and malloc function

#include<stdio.h>
#include<stdlib.h>

void fun() {

	int *ptr1 = (int*)malloc(sizeof(int));
	*ptr1 = 10;

	int* ptr2 = (int*)malloc(sizeof(int));
	*ptr2 = *ptr1;

	free(ptr1);

	printf("ptr1 = %d\n",*ptr1);
	printf("ptr2 = %d\n",*ptr2);
}

void main() {

	fun();
}
