//WAP to allocate memeory for salary n friends using calloc

#include<stdio.h>
#include<stdlib.h>

void main() {

	int *ptr = (int*)calloc(3,sizeof(int));

	for(int i = 1; i <= 3; i++ )
		*(ptr + i) = i*10000;

	for(int i = 1; i <= 3; i++ )
		printf("Salary of %d employe = %d\n",i,*(ptr+i));
}
