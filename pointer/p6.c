
//addition operation on pointer

#include<stdio.h>

void main() {

	int x = 10;
	int y = 11;

	int *xptr = &x;
	int *yptr = &y;
	xptr++;
	yptr--;

	printf("%d\n",*xptr);
	printf("%d\n",*yptr);

	printf("%d\n",*xptr+*yptr);
/*	printf("%d\n",*(xptr+yptr));

	printf("%p\n",xptr+yptr);
        printf("%p\n",xptr+xptr);
*/
}
