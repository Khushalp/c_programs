
#include<stdio.h>

void main() {

	int num = 0;

	printf("Enter number : ");
	scanf("%d",&num);

	int isEven = 0;

	if( num % 2 == 0 )
		isEven = 1;

	while( num >= 1 ) {
	
		printf("%d ",num);

		if(isEven) 
			num--;

		else
			num -= 2;
	}

	printf("\n");
}
