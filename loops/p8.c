
#include<stdio.h>

void main() {

	int num = 0;

	printf("Enter a number : ");
	scanf("%d",&num);

	if( num % 2 == 0 ) {
	
		while( num != 0 ) {
		
			printf("%d ",num);
			num--;
		}

		printf("\n");
	
	} else {
	
		while( num >= 0 ) {
		
			printf("%d ",num);
			num -= 2;
		}

		printf("\n");
	}
}
