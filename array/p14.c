//sum of rows in 2d array

#include<stdio.h>

void main() {

	int row, col;

	printf("Enter row and column : \n");
	scanf("%d%d",&row,&col);

	int arr[row][col];

	printf("Enter array elements : \n");

	for(int i = 0; i < row; i++ ) {
	
		for(int j = 0; j < col; j++ )
			scanf("%d",&arr[i][j]);
	}

	printf("Addition of rows : \n");
	
	for(int i = 0; i < row; i++ ) {

		for(int j = 0; j < col; j++ ) {
		
			printf("%d ",arr[i][j]+arr[i+1][j]);
		}
		
		printf("\n");
	}

	printf("\n");
}
