
//comparing array

#include<stdio.h>

void main() {

	int arr1[3] = {10,20,30};
	int arr2[3] = {10,20,30};

	if(arr1 == arr2)				//check address
		printf("Array are equal\n");
	else
		printf("Array are not equal\n");

//compare array using element

	int flag = 0;

	for(int i = 0; i < 3; i++ ) {
	
		if(arr1[i] ==  arr2[i]) {		//check element wise
		
			flag++;
			break;
		}
	}

	if(flag) 
		printf("Array element are same\n");
	else
		printf("Array element are not same\n");
}
