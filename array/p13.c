//2D array

#include<stdio.h>

void main() {

	int row, col;

	printf("Enter array row and column : \n");
	scanf("%d%d",&row,&col);

	int arr[row][col];

	printf("Enter array element : \n");

	for(int i = 0; i < row; i++ ) {
	
		for(int j = 0; j < col; j++)
			scanf("%d",&arr[i][j]);
	}

	printf("2D array elements are : \n");

	for(int i = 0; i < row; i++ ) {
	
		for(int j = 0; j < col ; j++ ) {
		
			printf("%d ", arr[i][j]);
		}

		printf("\n");
	}
}
