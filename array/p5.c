//print array element which are divided by 2 and count of it

#include<stdio.h>

void main() {

	int arr[5];

	printf("Enter array element : \n");
	
	for(int i = 0; i < 5; i++ ) 
		scanf("%d",&arr[i]);

	printf("Array divided by 2 are : \n");

	int count = 0;

	for(int i = 0; i < 5; i++ ) {
	
		if(arr[i]%2 == 0) {

			printf("%d ",arr[i]);
			count++;
		}
	}

	printf("\nCount is %d\n",count);

}
