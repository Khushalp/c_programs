
//taking size of array from user and search an element

#include<stdio.h>

void main() {

	int size;

	printf("Enter array size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element : \n");

	for(int i = 0; i < size; i++ ) 
		scanf("%d",&arr[i]);

	int search = 0, flag = 0;

	printf("Enter element to search : ");
	scanf("%d",&search);

	for(int i = 0; i < size; i++ ) {
	
		if(search == arr[i])
			flag = 1;
	}

	if(flag = 1)
		printf("Element Found\n");
	else
		printf("Element Not Found\n");
}

