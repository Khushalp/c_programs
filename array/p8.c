
//count of array element divide by 4 and 5

#include<stdio.h>

void main() {

	int size;

	printf("Enter size of array : ");
	scanf("%d",&size);

	int arr[size], count = 0;

	printf("Enter array element : \n");

	for(int i = 0; i < size; i++ ) {
	
		scanf("%d",&arr[i]);

		if((arr[i]%4 == 0) && (arr[i]%5 == 0))
			count++;
	}

	printf("Count of element divided by 4 and 5 is : %d\n",count);
}
