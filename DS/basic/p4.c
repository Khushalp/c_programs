
#include<stdio.h>
#include<string.h>

struct OTT {

	char pName[10];
	int userCount;
	float price;
};

void main() {

	struct OTT *ptr1 = (struct OTT*)malloc(sizeof(struct OTT));

	strcpy(ptr1->pName,"PrimeVideo");
	ptr1->userCount = 10000;
	ptr1->price = 350.50;

	printf("Name = %s\n",ptr1->pName);
	printf("User Count = %d\n",ptr1->userCount);
	printf("Price = %f\n",ptr1->price);
}
