//connectiong through newnode

#include<stdio.h>
#include<stdlib.h>

typedef struct Student {

	int id;
	float ht;
	struct Student *next;

}stud;

void printData(stud *head) {

	stud *temp = head;

	while(temp != NULL) {
	
		printf("%d\n",temp->id);
		printf("%f\n",temp->ht);
		temp = temp->next;
	}
}

void main() {

	stud *head = NULL;

	stud *newNode = (stud*)malloc(sizeof(stud));

	newNode->id = 1;
	newNode->ht = 4.5;
	newNode->next = NULL;

	head = newNode;

	newNode = (stud*)malloc(sizeof(stud));

	newNode->id = 2;
	newNode->ht = 5.5;
	newNode->next = NULL;

	head->next = newNode;

	newNode = (stud*)malloc(sizeof(stud));

	newNode->id = 3;
	newNode->ht = 6.5;
	newNode->next = NULL;

	head->next->next = newNode;

	printData(head);
}
