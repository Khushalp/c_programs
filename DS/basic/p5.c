//real time example of assigning data to structure using malloc by self referencing pointer

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct employee {

	int empId;
	char empName[20];
	float sal;
	struct employee *next;

}Emp;

void main() {

	Emp *emp1 = (Emp*)malloc(sizeof(Emp));
	Emp *emp2 = (Emp*)malloc(sizeof(Emp));
	Emp *emp3 = (Emp*)malloc(sizeof(Emp));

	printf("Size = %d\n\n",sizeof(Emp));

	emp1->empId = 1;
	strcpy(emp1->empName, "Khushal");
	emp1->sal = 35.00;
	emp1->next = emp2;

	emp2->empId = 2;
	strcpy(emp2->empName, "Sanket");
	emp2->sal = 40.00;
	emp2->next = emp3;

	emp3->empId = 3;
	strcpy(emp2->empName, "Prasanna");
	emp3->sal = 45.00;
	emp3->next = NULL;

	printf("EmpId = %d\n",emp1->empId);
	printf("EmpName = %s\n",emp1->empName);
	printf("sal = %f\n",emp1->sal);
	printf("Address1 = %p\n\n",emp1->next);

	printf("EmpId = %d\n",emp2->empId);
	printf("EmpName = %s\n",emp2->empName);
	printf("sal = %f\n",emp2->sal);
	printf("Address2 = %p\n\n",emp2->next);

	printf("EmpId = %d\n",emp3->empId);
	printf("EmpName = %s\n",emp3->empName);
	printf("sal = %f\n",emp3->sal);
	printf("Address = %p\n",emp3->next);

}
