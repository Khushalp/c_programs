//add node and print node using function

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct smartPhone{

	int ram;
	char pName[10];
	float price;
	struct smartPhone *next;

}sPhone;

sPhone *head = NULL;

void addNode() {

	sPhone *newNode = (sPhone*)malloc(sizeof(sPhone));

	newNode->ram = 3;
	strcpy(newNode->pName,"realme");
	newNode->price = 8000.00;
	newNode->next = NULL;

	head = newNode;
}

void printNode() {

	sPhone *temp = head;

	while(temp != NULL) {
	
		printf("%d\n",temp->ram);
		printf("%s\n",temp->pName);
		printf("%f\n",temp->price);
		temp = temp->next;
	}
}

void main() {

	addNode();
	printNode();
}
