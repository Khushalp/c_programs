#include<stdio.h>
#include<stdlib.h>

typedef struct Movie {

	char mName[20];
	float imdb;
	struct Movie *next;
}mv;

mv *head = NULL;

void addNode() {

	mv *newNode = (mv*)malloc(sizeof(mv));

	printf("Enter movie name : ");
	fgets(newNode->mName,15,stdin);

	printf("Enter imdb rating : ");
	scanf("%f",&(newNode->imdb));

	getchar();

	if(head == NULL) {
	
		head = newNode;

	} else {
	
		mv *temp = head;
		
		while(temp->next != NULL)
			temp = temp->next;

		temp->next = newNode;
	}
}

void printNode() {

	mv *temp = head;

	while(temp != NULL) {
	
		printf("%s",temp->mName);
		printf("%f\n",temp->imdb);
		temp = temp->next;
	}
}

void main() {

	addNode();
	addNode();
	printNode();
}
