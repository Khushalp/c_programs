
#include<stdio.h>
#include<stdlib.h>

typedef struct Student {

	int id;
	float ht;
	struct Student *next;
}stud;

stud *addNode(stud **head) {

	stud *newNode = (stud*)malloc(sizeof(stud));

	newNode->id = 1;
	newNode->ht = 5.5;
	newNode->next = NULL;

	return newNode;
}

void printNode(stud *head) {

	stud *temp = head;

	while(temp != NULL) {
	
		printf("%d\n",temp->id);
		printf("%f\n",temp->ht);
		temp = temp->next;
	}
}

void main() {

	stud *head = NULL;

	addNode(&head);
	printNode(head);
}
