//access data and enter data using function 

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Movie {

	int noOfTik;
	char mName[10];
	float price;
	struct Movie *next;
}mv;

void accessData(mv *aptr) {

	printf("%d\n",aptr->noOfTik);
	printf("%s\n",aptr->mName);
	printf("%f\n",aptr->price);
	printf("%p\n",aptr->next);
}

/*
void enterData(mv *eptr) {

        scanf("%d\n",&(eptr->noOfTik));
        scanf("%s\n",&(eptr->mName));
        scanf("%f\n",&(eptr->price));
        //eptr->next = eptr;
}
*/

void main() {

	mv *mv1 = (mv*)malloc(sizeof(mv));
	mv *mv2 = (mv*)malloc(sizeof(mv));

	mv1->noOfTik = 2;
	strcpy(mv1->mName, "RRR");
	mv1->price = 420.00;
	mv1->next = mv2;

	mv2->noOfTik = 3;
        strcpy(mv2->mName, "Kantara");
        mv2->price = 520.00;
        mv2->next = NULL;


	enterData(mv1);
	enterData(mv2);

	accessData(mv1);
	accessData(mv2);
}
