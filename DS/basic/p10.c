//input data 

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Demo {

	int x;
	struct Demo *next;
}Demo;

Demo *head = NULL;

void inputData() {

	Demo *newNode = (Demo*)malloc(sizeof(Demo));

	printf("Enter x : ");
	scanf("%d",&(newNode->x));
	
	newNode->next = NULL;
	head = newNode;
}

void printData() {

	Demo *temp = head;

	while(temp != NULL) {
	
		printf("%d\n",temp->x);
		temp = temp->next;
	}
}

void main() {

	inputData();
	printData();
}
