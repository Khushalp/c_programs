
/*
 *take no of rows from the user

	D	e	F	g
	e	D	c	B
	F	g	H	i
	g	F	e	D
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	for(int i = 0; i < range; i++ ) {

		char ch1 = range+64+i;
		char ch2 = ch1+32;

		for(int j = 0; j < range; j++ ) {
		
			if( (i+j)%2 == 0 ) {
			
				printf("%c\t",ch1);
			
			} else {
			
				printf("%c\t",ch2);
			}

			ch1++;
			ch2++;
		}

		printf("\n");
	}
}
