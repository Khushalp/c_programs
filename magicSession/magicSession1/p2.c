
/*
 * WAP to count digits in given no
Input: 94211
Output: digit count is 5
*/

#include<stdio.h>

void main() {

	int num;

	printf("Enter number : ");
	scanf("%d",&num);

	int count = 0;

	while(num > 0) {
	
		num /= 10;

		count++;
	}

	printf("digit count is %d\n",count);
}
