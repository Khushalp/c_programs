
/*
 *take no of rows from the user
 
 D	D	D	D
	C	C	C
		B	B
			A
 */

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	char ch = 64+range;

	for(int row = 0; row < range ; row++ ) {

		for(int space = 1; space <= row; space++ )
			printf("\t");

		for(int col = range; col > row; col-- )
			printf("\t%c",ch);

		printf("\n");
		ch--;

	}
}
