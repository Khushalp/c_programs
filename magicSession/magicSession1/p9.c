
/*
 * take no of rows from the user

 		1
	     1	2
	  1  2	3
	1 2  3	4
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	for(int i = 1; i <= range; i++ ) {
	
		for(int sp = range; sp > i; sp-- ) 
			printf("  ");

		for(int j = 1; j <= i; j++ )
			printf(" %d",j);

		printf("\n");
	}
}
