
/*
 * take no of rows from the user
 *
 * 	0
 * 	1	1	
 * 	2	3	5
 * 	8	13	21	34
 *
 * */

#include<stdio.h>

void main() {

	int past = 0, present = 1, future = 0;
	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	for(int i = 0; i < range; i++ ) {
	
		for(int j = 0; j <= i; j++ ) {
		
			while(present < range*range ) {
				
				// 0 1 1 2 3 5 8 

				future = past;	
				printf("%d ",past);
				present = past;
				past += present;
			}
		}

		printf("\n");
	}
}
