
/*
 * WAP to print all the composite numbers between a given range
Input : 
Enter Start 1
Enter End 15
Output:
4 6 8 9 10 12 14 15

Input : 
Enter Start 31
Enter End 35
Output:
32 33 34 35
*/

#include<stdio.h>

void main() {

	int start = 0, end = 0;

	printf("Enter start and end : \n");
	scanf("%d%d",&start,&end);

	for(int i = 2; i <= end; i++ ) {
	
		int flag = 0;

		for(int j = i-1; j > 1; j-- ) {
		
			if( i % j == 0 )
				flag = 1;
		}

		if(flag == 1)
			printf("%d ",i);
	}

	printf("\n");
}












