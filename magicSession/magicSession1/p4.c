
/*
 * take no of rows from the user
* - - -
- * - -
- - * -
- - - *

*/

#include<stdio.h>

void main() {

	int row = 0, col = 0;

	printf("Enter row and column : \n");
	scanf("%d%d",&row,&col);

	for(int i = 0; i < row; i++ ) {
	
		for(int j = 0; j < col; j++ ) {
		
			if( i == j ) 
				printf("* ");
			else
				printf("- ");
		}

		printf("\n");
	}
}
