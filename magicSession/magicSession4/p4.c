//WAP  that dynamically allocates a 3D array of integers, takes values from the user and print it[use malloc()] 
// plane = 2	row = 2		column = 3

#include<stdio.h>
#include<stdlib.h>

void main() {

	int p, r, c;

	printf("Enter plane, row and column of 3d array :\n");
	scanf("%d%d%d",&p,&r,&c);

	int* arr = (int*)malloc(sizeof(int)*p*r*c);
	int* arr1 = arr;

	printf("Enter array element : \n");

	for(int i = 0; i < p*r*c; i++ ) 
		scanf("%d", arr++);

	printf("Array element : \n");

	for(int i = 0; i < p; i++ ) {
	
		for(int j = 0; j < r; j++ ) {
		
			for(int k = 0; k < c; k++ ) {
			
				printf("%d ",*arr1);
				arr1++;
			}

			printf("\n");
		}

		printf("\n");
	}
}
