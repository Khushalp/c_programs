//WAP that dynamically allocates a 2D array of integer, take elements from user and print it [use malloc()]
//row = 3	column = 4

#include<stdio.h>
#include<stdlib.h>

void main() {

	int row, col;

	printf("Enter row and column : \n");
	scanf("%d%d",&row,&col);

	int* arr = (int*)malloc(sizeof(int)*row*col);
	int* arr1 = arr;

	printf("Enter 2D array element  : \n");

	for(int i = 0; i < row*col; i++){
	
		scanf("%d",arr++);
		//arr += i;
	}

	printf("Array element : \n");

	for(int i = 0; i < row; i++ ) {
	
		for(int j = 0; j < col; j++) {
		
			printf("%d ", *arr1);
			arr1++;
		}

		printf("\n");
	}
}
