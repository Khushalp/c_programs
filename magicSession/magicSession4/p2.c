//WAP that dynamically allocates a 1-D Array of marks, takes value from the user and prints it. [use malloc()]

#include<stdio.h>
#include<stdlib.h>

void main() {

	int size;

	printf("Enter size :");
	scanf("%d",&size);

	int* ptr = (int*)malloc(sizeof(int)*size);
	int* ptr1 = ptr;

	printf("Enter array elements : \n");

	for(int i = 0; i < size; i++)
		scanf("%d",ptr++);

	printf("Array element :\n");

	for(int i = 0; i < size; i++)
		printf("%d \n",*ptr1++);

	printf("\n");
}
