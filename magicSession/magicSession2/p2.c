
/*
 * WAP to calculate the count of even and odd elements
Take array size and array elements from the user
IP : enter array : 10 12 13 15 16 17 19 20 22 23
OP: even element count is
OP: odd element count is
*/

#include<stdio.h>

void main() {

	int size;

	printf("Enter array size : ");
	scanf("%d",&size);

	int arr[size];
	int even = 0,odd = 0;

	printf("Enter array element : \n");

	for(int i = 0; i < size; i++ ) {

		scanf("%d",&arr[i]);

		if( arr[i] % 2 == 0 )
			even++;
		else
			odd++;
	}

	printf("Even element count = %d \n Odd element count = %d \n",even,odd);
}
