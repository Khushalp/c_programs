
/*
 * WAP to swap values of two numbers using a pointer.
(Hint: Use de-referencing of pointers)
Input : x=10y=20
Op: After swapping
x=20
y=10
*/

#include<stdio.h>

void main() {

	int x,y;

	ptintf("ENter x and y : \n");
	scanf("%d%d",&x,&y);

	int *xptr = &x;
	int *yptr = &y;

	int temp = 0;

	temp = *xptr;
	*xptr = *yptr;
	*yptr = *xptr;

	printf("x = %d\n",*xptr);
	printf();
}
