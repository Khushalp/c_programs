
/*
WAP to find the given element from the array
Take array size and array elements from the user
IP : enter array : 10 12 13 15 16 14
Ip : enter element : 15
Op: 15 is present
*/

#include<stdio.h>

void main() {

	int size;

	printf("Enter size of an array : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element : \n");

	for(int i = 0; i < size; i++ )
		scanf("%d",&arr[i]);

	int search;

	printf("ENter search element : ");
	scanf("%d",&search);

	int flag;

	for(int i = 0; i < size; i++ ) {
	
		if(arr[i] == search) {
		
			flag = 1;
			break;

		} else {
			
			flag = 0;
		}
			
	}

	if(flag == 1)
		printf("Element is Present\n");
	else
		printf("Element is not Present\n");
}
