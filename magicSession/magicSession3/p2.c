//WAP to accept number from user seperate digits number & enter them in an Array then 
//sort the array in ascending order
// input : 7364598210                   7 	3	2
// output : 0123456789

#include<stdio.h>

int sortArr(int *arr, int len) {

	int c = len;

	while( c > 0 ) {

		for(int i = 0; i < len; i++ ) {
	
			if( arr[i] > arr[i+1] ){
			
				int temp = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = temp; 
			}
		}
		c--;
	}
}

void main() {

	long num;

	printf("Enter a number : ");
	scanf("%ld",&num);

	long len = 0, temp = num;

	while(temp != 0) {
	
		temp /= 10;
		len++;
	} 

	printf("Length = %ld\n",len);

	int arr[len];
	long temp1 = num;

	for(int i = 0; i < len; i++ ) {
		
		int rem = temp1%10;
		temp1 /= 10;
		arr[i] = rem;
	}

	printf("\n");

	sortArr(arr,len);

	for(int i = 0; i < len; i++ )
		printf("%d |",arr[i]);

	printf("\n");
}
