//WAP to take two string from user and concat them using myStrCon()

#include<stdio.h>

char myStrCon(char *str1, char *str2) {

	while(*str1 != '\0')
		str1++;

	while(*str2 != '\0') {
	
		*str1 = *str2;
		str1++;
		str2++;
	}

	*str1 = '\0';
}

void main() {

	char *str1[] = {"a"};
	char *str2[] = {"b"};

	printf("ENter two string : \n");
	gets(str1);
	gets(str2);

	myStrCon(str1,str2);

	printf("%s \n",str1);
}
