//WAP to take size and element of array and print cube of even element and square of odd element

#include<stdio.h>

void main() {

	int size;

	printf("Enter length : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element : \n");

	for(int i = 0; i < size; i++ )
		scanf("%d",&arr[i]);

	for(int i = 0; i < size; i++ ) {
	
		int temp = arr[i];

		if(arr[i]%2 == 0)
			arr[i] = temp*temp*temp;
		else
			arr[i] = temp*temp;
	}

	for(int i = 0; i < size; i++ )
		printf("%d ",arr[i]);

	printf("\n");
}
