//WAP that accepts a String from user and print its length using mystrlen()

#include<stdio.h>
#include<string.h>

int myStrLen(char *str) {

	int len = 0;

	while(*str != '\0') {
	
		str++;
		len++;
	}

	return len;
}

void main() {

	char* str[1] ={"a"} ;

	printf("Enter a string : ");
	gets(str);

	printf("Length = %d\n",myStrLen(str));
}
