//string compare

#include<string.h>
#include<stdio.h>

int mystrcmp(char *str1, char *str2) {

	while(*str2 != '\0') {
	
		if(*str1 == *str2) {
		
			str1++;
			str2++;
		}
		else {

			return *str1 - *str2;
		}
	}

	return 0;
}

void main() {

	char *str1 = "Khushal";
	char str2[20] = {'k','h','u','s','h','a','l','\0'};

	puts(str1);
	puts(str2);

	int diff = 1;

	//printf("%d\n",strcmp(str1,str2));
	
	if(strlen(str1) == strlen(str2)) 
		diff = mystrcmp(str1,str2);

	if(diff == 0)
		printf("String are equal\n");
	else
		printf("String are not equal\n");


}

