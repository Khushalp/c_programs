//string compare ignoreing case(upper and lower)

#include<stdio.h>
#include<string.h>

int mystrcmpi(char* str1,char *str2) {

	while(*str1 != '\0') {
	
		if(*str1 == *str2 || *str1 - *str2 == 32 || *str2 - *str1 == 32) {
		
			str1++;
			str2++;
		}
		else{
		
			return str1 - str2;
		}
	}

	return 0;
}

void main() {

	char str1[20] = {'A','b','C','\0'};
	char *str2 = "aBc";

	puts(str1);
	puts(str2);

	int diff = 1;

	if(strlen(str1) == strlen(str2))
		diff = mystrcmpi(str1,str2);

	if(diff == 0)
		printf("String are equal\n");
	else
		printf("String are not equal\n");
}
