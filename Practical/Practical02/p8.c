
//WAP to print the product of digits

#include<stdio.h>

void main() {

	int num = 0;

	printf("Enter a number : ");
	scanf("%d",&num);

	int temp = num;
	int rem = 0, prod = 1;

	while( num > 0 ) {
	
		rem = num % 10;
		prod *= rem;
		num /= 10;
	}

	printf("%d is product of digit of %d\n",prod,temp);
}
