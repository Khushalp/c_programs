
//Take an input from user  and print the number in reverse

#include<stdio.h>

void main() {

	int num = 0;

	printf("Enter a number : ");
	scanf("%d",&num);

	int rem = 0;

	printf("Reverse of %d is : ",num);

	while( num > 0) {
	
		rem = num % 10;
		
		printf("%d",rem);

		num /= 10;
	}

	printf("\n");
}
