
//WAP to print whether that is a prime number or not

#include<stdio.h>

void main() {

	int num = 0,count = 0;

	printf("Enter a number : ");
	scanf("%d",&num);

	for(int i = 2; i <= (num-1); i++ ) {

		if( num % i == 0 ) 
			count++;
	}

	if(count == 0)
		printf("%d is a prime number\n",num);

	else
		printf("%d is not a prime number\n",num);
}
