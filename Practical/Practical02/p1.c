
//WAP to print the sum of numbers which is not divisible by 3 upto given number

#include<stdio.h>

void main() {

	int num = 0, sum = 0;

	printf("Enter a number : ");
	scanf("%d",&num);

	for(int i = 1; i <= num; i++ ) {
	
		if( i % 3 != 0 ) {
		
			sum += i;
		}
	}

	printf("Sum of numbers is %d\n",sum);
}
