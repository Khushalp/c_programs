
//Take a number from user and print the fibonacci series upto that number

#include<stdio.h>

void main() {

	int num = 0;

	printf("Enter a number to print fibonacci series : ");
	scanf("%d",&num);

	int past = 0;
	int present = 1;

	while( past < num ) {

		printf("%d ",past);
		past += present;
		present = past - present;		

	}

	printf("\n");
}
