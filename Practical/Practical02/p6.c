
//WAP to print the digit of a number

#include<stdio.h>

void main() {

	int num = 0, count = 0;
	int rem = 0;

	printf("Enter a number : ");
	scanf("%d",&num);

	int temp = num;

	while( num > 0) {
	
		rem = num % 10;
		num /= 10;
		
		count++;
	}

	printf("%d is digit of %d number\n",count,temp);
}
