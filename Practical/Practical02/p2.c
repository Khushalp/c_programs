
//WAP to print the addition of 1 to 10 with 10 to 1

#include<stdio.h>

void main() {

	int num = 10, add = 0;

	for(int i = 1; i <= 10; i++ ) {
	
		printf("%d + %d = %d\n",i,num,i+num);
		num--;
	}
}
