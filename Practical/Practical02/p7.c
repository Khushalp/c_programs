
//WAP to print the sum of digit

#include<stdio.h>

void main() {

	int num = 0;
	int rem = 0, sum = 0;

	printf("Enter a number : ");
	scanf("%d",&num);

	int temp = num;

	while( num > 0 ) {
	
		rem = num % 10;
		sum += rem;
		num /= 10;
	}

	printf("%d is the sum of digit of %d\n",sum,temp);
}
