
/*
Write a program, take two characters if these characters are equal then print them as it is
but if they are unequal then print their difference.
Input: va1=s var2=s
Output: va1=s var2=s
Input: va1=a var2=p
Output: The difference between a and p is 15
*/

#include<stdio.h>

void main() {

	char ch1, ch2;
	int diff = 0;

	printf("Enter two character : ");
	scanf(" %c %c",&ch1,&ch2);

	if( ch1 > ch2 ) {

		printf("The difference between %c and %c is %d\n",ch1,ch2,ch1-ch2);
	
	} else if( ch2 > ch1 ) {
	
		printf("The difference between %c and %c is %d\n",ch1,ch2,ch2-ch1);
	
	} else {
	
		printf("ch1 = %c  ch2 = %c\n",ch1,ch2);
	}
}
