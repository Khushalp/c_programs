
/*Write a program, in which according to the month number
print the no. of days in that month
(use switch case)
Input : month = 7
Output: July has 31 days
*/

#include<stdio.h>

void main() {

	int month = 0;

	printf("Enter a month to find out days in it : ");
	scanf("%d",&month);

	switch(month) {
	
		case 1:
			printf("%d month has 31 days\n",month);
			break;

		case 2:
                        printf("%d month has 28 days and 29 days in a leap year\n",month);
                        break;
			
		case 3:
                        printf("%d month has 31 days\n",month);
                        break;
 		
		case 4:
                        printf("%d month has 30 days\n",month);
                        break;
 
		case 5:
                        printf("%d month has 31 days\n",month);
                        break;

	       	case 6:
                        printf("%d month has 30 days\n",month);
                        break;

	       	case 7:
                        printf("%d month has 31 days\n",month);
                        break;

	       	case 8:
                        printf("%d month has 31 days\n",month);
                        break;

	       	case 9:
                        printf("%d month has 30 days\n",month);
                        break;

	       	case 10:
                        printf("%d month has 31 days\n",month);
                        break;

	       	case 11:
                        printf("%d month has 30 days\n",month);
                        break;

	       	case 12:
                        printf("%d month has 31 days\n",month);
                        break;

	       	 default:
                        printf("Enter valid number of month\n");
                        break;
	}
}

