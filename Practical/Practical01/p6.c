
/*
WAP to get 10 numbers from Users and find their sum and average.
IP: 1 2 3 4 5 6 7 8 9 10
OP :
sum is 55
Average is 55.50
*/

#include<stdio.h>

void main() {

	int num = 0, sum = 0;

	printf("Enter a range of number : ");
	
	for(int i = 0; i < 10; i++) {
	
		scanf("%d",&num);

		sum = sum + num;
	}

	printf("%d is sum of number\n",sum);
	printf("%d is average of number\n",sum/10);
}
