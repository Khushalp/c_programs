
/*WAP to find min among 3 numbers.
 *Input : 2 4 3
 *Output: minimum number is 2
 */

#include<stdio.h> 

void main() {

	int a,b,c;

	printf("Enter 3 numbers to find min : ");
	scanf("%d%d%d",&a,&b,&c);

	if( a < b && a < c ) 
		printf("%d is min\n",a);

	else if( b < c ) 
		printf("%d is min\n",b);

	else if( c < a ) 
		printf("%d is min\n",c);

	else
		printf("Entered values are equal\n");
}
