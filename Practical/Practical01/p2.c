
/*
 *WAP to find max among 3 numbers.
 *Input : 2 3 4
 *Output: max number is 4
 */

#include<stdio.h>

void main() {

	int a,b,c;

	printf("Enter 3 number to find max : ");
	scanf("%d%d%d",&a,&b,&c);

	if( a > b && a > c ) 
		printf("%d is max\n",a);

	else if( b > c ) 
		printf("%d is max\n",b);

	else if( c > a ) 
		printf("%d is max\n",c);

	else
		printf("All values are equal\n");
}
