
/*
Write a program that takes a number from 1 to 5 and prints its
spelling,
if the number is greater than 5 print entered number is greater than 5
(use switch case)
Input : var4= 4
Output: four
Input : 9
Op: 9 is greater than 5
*/

#include<stdio.h>

void main() {

	int num = 0;

	printf("Enter a number to print it's spelling : ");
	scanf("%d",&num);

	switch(num) {
	
		case 1:
			printf("One\n");
			break;

		case 2:
                        printf("Two\n");
                        break;

	       	case 3:
                        printf("Three\n");
                        break;

	       	case 4: 
		       	printf("four\n");
                        break;
	       
		case 5:
                        printf("Five\n");
                        break;
 
		default:
                        printf("%d is greater than 5 OR smaller than 0 :\n",num);
	}
}
