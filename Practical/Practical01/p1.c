
/*
 *WAP to take check whether the input is a leap year or not (basic leap year)
 *Input: 2000
 *Output: 2000 is a leap year
 *Input: 1999
 *Output: 1999 is not a leap year
 */

#include<stdio.h>

void main() {

	int year = 0;

	printf("Enter a year to check it is a leap year or not : ");
	scanf("%d",&year);

	if(year % 4 == 0 && year > 0 ) 
		printf("%d is a leap year\n",year);

	else
		printf("%d is not a leap year\n",year);
}

