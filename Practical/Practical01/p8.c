
/*
Write a Program that takes the angles of a triangle from the user and
print whether that triangle is valid or not.
{Note: Addition of angles of the triangle has to be 180 in order to
consider it as a valid one}
Input: 30 60 70
Output: The triangle with angles 30 60 & 70 is an invalid one
*/

#include<stdio.h>

void main() {

	int a,b,c;

	printf("Enter angles of triangle : ");
	scanf("%d%d%d",&a,&b,&c);

	if( a+c+b == 180 )
		printf("The triangle with angles %d %d & %d is valid\n",a,b,c);
	
	else
		printf("The triangle with angles %d %d & %d is invalid\n",a,b,c);
}
