
/*
WAP to check whether the given input is a Pythagorean triplet or not
Note: Pythagorean triplet means if given three numbers a b and c follows this pattern
c*c = a*a + b*b
Any side can be hypotenuse
*/

#include<stdio.h>

void main() {

	int a,b,c;
	int x,y,z;

	printf("Enter sides of triangle to check pythagorous triplet : ");
	scanf("%d%d%d",&a,&b,&c);

	if( a > b && a > c ) {
	
		x = a;
		y = b;
		z = c;
	
	} else if( b > c ) {
	
		x = b;
		y = a;
		z = c;
	
	} else {

		x = c; 
		y = a;
		z = b;
	}

	if( x*x == y*y + z*z ) 
		printf("It is a Pythagorous triplet\n");

	else
		printf("It is not a Pythagrous triplet\n");
}

