
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct Company {

	char cName[20];
	int empCount;
	float cRevn;
};

void main() {

	struct Company cmp = {"xyz",10,10.00};

	//cmp obj = new cmp();

	//strcpy(cmp -> cName,("Biencaps"));
	//empCount = 25;
	//cRevn = 8.00;

	printf("%s\n",cmp.cName);
	printf("%d\n",cmp.empCount);
	printf("%f\n",cmp.cRevn);
}
