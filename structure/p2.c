//Write a real example of structure which we can implement in linked list

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Playlist {

    int noOfSong;
    char sName[10];
    float sTime;
    struct Playlist *nextSong;

}Pl;

void main() {

    Pl obj1, obj2, obj3;

    Pl *head = &obj1;

    head->noOfSong = 1;
    strcpy(head->sName,"abc");
    head->sTime = 2.45;
    obj1.nextSong = &obj2;

    obj1.nextSong->noOfSong = 2;
    strcpy(obj1.nextSong->sName,"pqr");
    obj1.nextSong->sTime = 3.50;
    obj2.nextSong = &obj3;

    obj2.nextSong->noOfSong = 3;
    strcpy(obj2.nextSong->sName,"xyz");
    obj2.nextSong->sTime = 4.15;
    obj3.nextSong = NULL;

    printf("Number of song = %d\n",head->noOfSong);
    printf("Name of song = %s\n",head->sName);
    printf("Time Duration of song = %f\n\n",head->sTime);

    printf("Number of song = %d\n",obj1.nextSong->noOfSong);
    printf("Name of song = %s\n",obj1.nextSong->sName);
    printf("Time Duration of song = %f\n\n",obj1.nextSong->sTime);
    
    printf("Number of song = %d\n",obj2.nextSong->noOfSong);
    printf("Name of song = %s\n",obj2.nextSong->sName);
    printf("Time Duration of song = %f\n\n",obj2.nextSong->sTime);
}
