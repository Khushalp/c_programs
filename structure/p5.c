//pragma pack

#include<stdio.h>
#pragma pack(8)

struct Demo{

	int x;
	char ch;
	char name[20];
	float y;
	double z;
};

void main() {

	printf("%d\n",sizeof(struct Demo));
}
