//size of structure

#include<stdio.h>

struct Demo{

	int jerNo;
	float avg;
	double grade;
};

struct Demo1{

	float f1;
	char *ch1;
	int x;
	int y;
};

struct Demo2{

	char ch;
	int x;
	float y;
	char arr[10];
};

void main() {

	struct Demo obj;
	struct Demo1 obj1;
	struct Demo2 obj2;

	printf("%d\n",sizeof(obj));
	printf("%d\n",sizeof(obj1));
	printf("%d\n",sizeof(obj2));
}
