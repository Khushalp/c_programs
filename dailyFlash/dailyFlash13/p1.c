/*
 * floyd number pattern
		1
	2		3
4		5		6
*/

#include<stdio.h>

void main() {

	int row = 0;

	printf("Enter number of row : ");
	scanf("%d",&row);

	int  x = 1;

	for(int i = 0; i < row; i++ ) {
	
		for(int s = (row-1); s > i; s-- )
			printf(" ");

		for(int j = 0; j <= i; j++ )
			printf("%d ",x++);

		printf("\n");
	}
}
