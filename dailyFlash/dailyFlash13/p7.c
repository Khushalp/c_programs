//print first 10 automorphic number

#include<stdio.h>

void main() {

	int num = 0;

	for(int i = 0; i < 10; ) {

		int temp = 0, count = 0;
		int x = num;

		while( x > 0 ) {
	
			int rem = x % 10;
			temp = (temp*10) + rem;
			x /= 10;
			count++;              
		}

		int temp1 = 0;
		int sqnum = num*num;

		while( count > 0) {

			int rem = sqnum % 10;
			temp1 = (temp1*10) + rem;
	     		sqnum /= 10;
			count--;	
		}

		if(temp == temp1) {
		
			printf("%d\n",num);
			i++;
		}

		num++;
	}
}
