/*
pascal series pattern
				1
			1		1
		1		2		1
	1		3		3		1
1		4		6		4		1
*/

#include<stdio.h>

void main() {

	int row = 0,x = 1;

	printf("Enter row : ");
	scanf("%d",&row);

	for(int i = 0; i < row; i++ ) {
	
		for(int s = (row-1); s > i; s-- )		
			printf(" ");

		printf(" %d",x);
		x *= 11;

		printf("\n");
	}
}
