
/*
automorphic number
	6 = 36 last digit is 6 = 6 so it is automorphic number
*/

#include<stdio.h>

void main() {

	int num = 0;

	printf("Enter number : ");
	scanf("%d",&num);

	int temp = 0, count = 0;
	int x = num;

	while( x > 0 ) {
	
		int rem = x % 10;
		temp = (temp*10) + rem;
		x /= 10;
		count++;              
	}

	int temp1 = 0;
	int sqnum = num*num;

	printf("Square of %d = %d\n",num,sqnum);

	while( count > 0) {

		int rem = sqnum % 10;
		temp1 = (temp1*10) + rem;
	     	sqnum /= 10;
		count--;	
	}

	if(temp == temp1)
		printf("Automorphic number\n");
	else
		printf("Not Automorphic number\n");
}
