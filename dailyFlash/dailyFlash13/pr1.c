/*
 * Floyd trangle
 * 		1
 * 	      2 3 4
 * 	    5 6 7 8 9
 * 	 10 11 12 13 14 15
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter no of rows :\n");
	scanf("%d",&row);

	int num=1; 
	for(int i=1; i<row*2; i+=2){
		for(int j=row*2; j>0; j--){
			if(i<j){
				printf(" ");
			}else{
				printf("%d ",num++);
			}
		}
		printf("\n");
	}
}


