
/*
	2	3	5
	7	11	13
	17	19	23
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	int x = 2;

	for(int i = 0; i < range; i++ ) {

		for(int j = 0; j < range; ) {

			int count = 0;
		
			for(int k = 2; k <= x; k++ ) {
			
				if( x % k == 0 )
				       count++;
			}

			if(count == 1) {
			
				printf("%d\t",x);
				j++;
			}
			x++;
		}

		printf("\n");
	}
}
