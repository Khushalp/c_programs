
/*
	1
	1	2
	2	3	4
	4	5	6	7
	7	8	9	10	11
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	int x = 1;

	for(int i = 0; i < range; i++ ) {
	
		for(int j = 0; j <= i; j++ ) {
		
			printf("%d\t",x++);
		}
		x--;
		printf("\n");
	}
} 
