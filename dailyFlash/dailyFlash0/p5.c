
/*
	1	4	7	10
	7	10	13	16
	13	16	19	22
	19	22	26	18
*/

#include<stdio.h>

void main() {

	int range;

	printf("Enter range : ");
	scanf("%d",&range);

	int x = 1;

	for(int i = 0; i < range; i++ ) {
	
		for(int j = 0; j < range; j++ ) {
		
			printf("%d\t",x);
			x += 3;
		}

		x -= 6;
		printf("\n");
	}
}
