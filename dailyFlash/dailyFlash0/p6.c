
//factorial of given number

#include<stdio.h>

void main() {

	int num = 0;

	printf("Enter number : ");
	scanf("%d",&num);

	int fact = 1;

	for(int i = 1; i <= num; i++ ) {
	
		fact *= i;
	}

	printf("Factorial of %d : %d\n",num,fact);
}
