
/*
	a
	b	B
	c	C	c
	d	D	d	D
*/

#include<stdio.h>

void main() {

	int range;

	printf("Enter range : ");
	scanf("%d",&range);

	char ch1 = 'a';
	char ch2 = 'A';

	for(int i = 0; i < range; i++ ) {
	
		for(int j = 0; j <= i; j++ ) {
			
			if(j % 2 == 0)
				printf("%c\t",ch1);
			else
				printf("%c\t",ch2);
		}

		ch1++;
		ch2++;
		printf("\n");
	}
}
