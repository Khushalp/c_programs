
/*
	2
	3	4
	4	5	6
	5	6	7	8
*/

#include<stdio.h>

void main () {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	int x = 0;

	for(int i = 0; i < range; i++ ) {
	
		x = i+2;

		for(int j = 0; j <= i; j++ ) {
		
			printf("%d\t",x++);
		}

		printf("\n");
	}
}
