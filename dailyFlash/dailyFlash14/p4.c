/*
 *WAP to replace every occurance of 1 by 2
 *
 *	I/P : 942111423
 *	O/P : 942222423
 * */

#include<stdio.h>

void main() {

	long num = 0;

	printf("Enter number :");
	scanf("%ld",&num);

	long rem, temp = 0, x = 1;

	long num1 = num;

	while(num1 > 0) {
	
		rem = num1 % 10;
		if(rem == x)
			rem = 2;

		temp = (temp*10) + rem;
		num1 /= 10;
	}

	long temp1 = 0;

	//printf("%ld %ld",temp,num);
	while( temp > 0) {
	
		rem = temp % 10;
		temp1 = (temp1*10) + rem;
		temp /= 10;
	}

	printf(" = %ld\n",temp1);
}
