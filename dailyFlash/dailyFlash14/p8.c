
#include<stdio.h>
#include<string.h>

int myStrlen(char *str){

	int count = 0;

	while(*str != '\0') {
	
		count++;
		str++;
	}

	return count;
}

void main() {

	char str[20];

	gets(str);

	int l = myStrlen(str);

	for(int i = 0; i < l; i++ ) {
	
		int y = i;

		for(int s = 1; s <= i; s++)
			printf(" ");

		for(int j = l; j > i; j-- ) {
		
			printf("%c",str[y]);
			y++;
		}

		printf("\n");
	}
}
