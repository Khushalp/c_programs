
/*
	4	5	6	7
	8	10	12	13
	12	15	18	21
	16	20	24	28
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	int x = 0;

	for(int i = 1; i <= range; i++ ) {
	
		x += range;
		int y = x;

		for(int j = 0; j < range; j++ ) {
		
			printf("%d\t",y);
			y += i;
		}

		printf("\n");
	}
}
