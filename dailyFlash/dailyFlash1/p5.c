
//fibo series

#include<stdio.h>

void main() {

	int range;

	printf("Enter range : ");
	scanf("%d",&range);

	int pre = 0, post = 1;
	int sum = 0;

	for(int i = 0; i < range; i++ ) {
	
		printf("%d ",pre);
		sum = pre + post;
		pre = post;
		post = sum;
	}

	printf("\n");
}
