
/*
	4	9	14	19
	8	14	20	26
	12	19	26	33
	16	29	32	40
*/

#include<stdio.h>

void main() {

	int range;

	printf("Enter range : ");
	scanf("%d",&range);

	int x = 0;

	for(int i = 0; i < range; i++ ) {
	
		x += range; 
		int y = x;

		for(int j = 0; j < range; j++ ) {
		
			printf("%d\t",y);
			y = y + 5 + i;
		}

		printf("\n");
	}
}
