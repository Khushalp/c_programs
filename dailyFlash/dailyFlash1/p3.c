
/*
	3	b	2	d
	b	2	d
	1	d
	d
*/

#include<stdio.h>

void main() {

	int range;

	printf("Enter range : ");
	scanf("%d",&range);

	for(int i = 1; i <= range; i++ ) {
	
		char ch = 96+i;
		char ch1 = ch;
		int x = range-i;

		for(int j = range; j >= i; j-- ) {
		
			if( (j+i) % 2 == 0 )
				printf("%c\t",ch1);
			else
				printf("%d\t",x--);
			
			ch1++;
		}

		printf("\n");
	}
}
