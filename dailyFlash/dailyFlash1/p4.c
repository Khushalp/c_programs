
/*
	1	b	2	d
		1	b	2
			1	b
				1
*/

#include<stdio.h>

void main() {

	int range = 0;

	printf("Enter range : ");
	scanf("%d",&range);

	for(int i = 0; i < range; i++ ) {
	
		for(int k = 0; k < i; k++ ) 
			printf(" \t");

		int x = 1;
		char ch = 'a';

		for(int j = range; j > i; j-- ) {
		
			if( j % 2 == 0 ) 
				printf("%d\t",x++);
			else
				printf("%c\t",ch);

			ch++;
		}

		printf("\n");
	}
}
